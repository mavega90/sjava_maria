class InfoNums {

    public static void main(String[] args) {
        
        int cuantos = args.length;
        if (cuantos==0) {
            System.out.println("No he recibido ningún número!");
            return;
        }

        int primero =  Integer.parseInt(args[0]);
        
        int total, mayor, menor;
        total = mayor = menor = primero;

        if (cuantos > 1) {
            for (int i = 1; i < cuantos; i++){
                int num = Integer.parseInt(args[i]);
                total += num;
                if (mayor<num) mayor=num;
                if (menor>num) menor=num;
            }
        }
        
        double media = (double) total / cuantos; // 'casteamos' total a double para que división sea double

        System.out.printf("%d números recibidos\n", cuantos);
        System.out.printf("El número mayor es %d\n", mayor);
        System.out.printf("El número menor es %d\n", menor);
        System.out.printf("La suma es %d\n", total);
        System.out.printf("La media es %.2f\n", media); // %.2f muestra float con dos decimales

    }

}
